
{
    'name': "Meat Center",
    'version': '15.0.1.0.0',
    'summary': """Meat Center.""",
    'description': """Meat Center , Odoo 15""",
    'author': "",
    'maintainer': '',
    'company': "",
    'website': "",
    'category': '',
    'depends': ['sale_management','purchase'],
    'data': [
        'data/data.xml',
        'security/ir.model.access.csv',
        #'views/product_template.xml',
        'views/purchase_lot.xml',
        'views/meat_center.xml',
        'views/meat_center_menu.xml'
        
    ],
    'images': [],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}
