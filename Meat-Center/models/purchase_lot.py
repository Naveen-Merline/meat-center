from odoo import models, fields, api, _
from datetime import datetime, timedelta

class PurchaseLot(models.Model):
    _name = 'purchase.lot'

    name = fields.Char('Purchase Lot',default='New')
    purchase_id = fields.Many2one('purchase.order',string="Purchase Order")
    product_id = fields.Many2one('product.template',string='Product')
    status = fields.Selection([('nu','Not Used'),('u','Used')],default='nu',string='Lot Status')
    
class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    
    purchase_lot_no_ids = fields.One2many('purchase.lot','purchase_id',string="Purchase Lot")
    
    def button_confirm(self,is_cut_material=None):
        if not is_cut_material:
            for order in self:
                if order.state not in ['draft', 'sent']:
                    continue
                order._add_supplier_to_product()
                # Deal with double validation process
                if order._approval_allowed():
                    order.button_approve()
                else:
                    order.write({'state': 'to approve'})
                if order.partner_id not in order.message_partner_ids:
                    order.message_subscribe([order.partner_id.id])
                    
            order_line_id = self.env['purchase.order.line'].search([('order_id','=',self.id)],limit=1)
            order_qty = int(order_line_id.product_qty)
            for i in range(order_qty):
                seq_no = self.env['ir.sequence'].next_by_code('purchase.lot') or _('New')
                purchase_lot_id = self.env['purchase.lot'].create({'name':seq_no,'purchase_id':self.id,'product_id':order_line_id.product_id.product_tmpl_id.id})
            stock_picking_id = self.env['stock.picking'].search([('origin','=',self.name)])
            stock_move_id = self.env['stock.move'].search([('picking_id','=',stock_picking_id.id)])
            stock_move_line_id = self.env['stock.move.line'].search([('move_id','=',stock_move_id.id)])
            for stock_move_line_details in stock_move_line_id:
                stock_move_line_details.unlink()
            stock_move_line_id = []
            if not stock_move_line_id:
                lot_ids = self.purchase_lot_no_ids
                for lot in lot_ids:
                    product_id = self.env['product.product'].search([('product_tmpl_id','=',lot.product_id.id)])
                    stock_id = self.env['stock.move.line'].create({'picking_id':stock_picking_id.id,'move_id':stock_move_id.id,'product_id':product_id.id,'qty_done':1,'lot_name':lot.name,'location_id':stock_move_id.location_id.id,'location_dest_id':stock_move_id.location_dest_id.id,'product_uom_id':stock_move_id.product_uom.id})
        else:
            for order in self:
                if order.state not in ['draft', 'sent']:
                    continue
                order._add_supplier_to_product()
                # Deal with double validation process
                if order._approval_allowed():
                    order.button_approve()
                else:
                    order.write({'state': 'to approve'})
                if order.partner_id not in order.message_partner_ids:
                    order.message_subscribe([order.partner_id.id])
        return True
    
class StockMoveLine(models.Model):
    _inherit = "stock.move.line"
    
    weight = fields.Float('Weight')
    
    