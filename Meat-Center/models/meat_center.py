from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    is_meat_center = fields.Boolean('Is Meat Center Sales')

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    
    is_meat_center = fields.Boolean('Is Meat Center Purchase')

class MeatCenter(models.Model):
    _name = 'meat.center'
    _rec_name = 'product_id'
    
    
    @api.model
    def default_get(self, fields):
        vals = super(MeatCenter, self).default_get(fields)
        is_add_qty = self.env.context.get('is_add_qty')
        vals['is_add_qty'] = is_add_qty
        return vals
    
    def consumed_bom(self,product_id):
        consumed_bom_value = []
        bom_id = self.env['mrp.bom'].search([('product_tmpl_id','=',product_id.id)],limit=1)
        if bom_id:
            bom_line_ids = bom_id.bom_line_ids
            for bom_line_id in bom_line_ids:
                consumed_bom_value.append((0,0,{'product_id':bom_line_id.product_tmpl_id.id,'product_qty':bom_line_id.product_qty,'total_weight':self.total_weight,'uom_id':bom_line_id.product_uom_id.id}))
        self.consume_bom_ids = consumed_bom_value
    
    def cut_meats(self):
        consumed_value = []
        bom_id = self.env['mrp.bom'].search([('product_tmpl_id','=',self.product_id.id)],limit=1)
        if bom_id:
            bom_line_ids = bom_id.bom_line_ids
            for bom_line_id in bom_line_ids:
                consumed_value.append((0,0,{'product_id':bom_line_id.product_tmpl_id.id,'product_qty':bom_line_id.product_qty,'total_weight':self.total_weight,'uom_id':bom_line_id.product_uom_id.id}))
                self.consumed_bom(bom_line_id.product_tmpl_id)
        self.consume_ids = consumed_value
            
        
    def sale_lines_value(self,sale_id):
        product = self.env['product.product']
        sale_values = []
        if self.product_id:
            product_id = product.search([('product_tmpl_id','=',self.product_id.id)])
            sale_values.append({'name':product_id.name,'product_id':product_id.id,'product_uom_qty':self.product_qty,'product_uom':product_id.uom_id.id,
                                'order_id':sale_id.id})
        return sale_values
    
    def purchase_lines_value(self,sale_id):
        product = self.env['product.product']
        sale_values = []
        if self.consume_ids:
            for consume_id in self.consume_ids:
                print(consume_id)
                product_id = product.search([('product_tmpl_id','=',consume_id.product_id.id)])
                sale_values.append({'name':product_id.name,'product_id':product_id.id,'product_qty':consume_id.product_qty,'product_uom':product_id.uom_id.id,
                                    'order_id':sale_id.id})
        return sale_values
    
    def update_inv_details(self,order_id):
        stokpicking = self.env['stock.picking']
        stockmove = self.env['stock.move']
        
        inv_id = stokpicking.search([('origin','=',order_id.name)])
        inv_line_ids = inv_id.move_ids_without_package
        
        for inv_line_id in inv_line_ids:
            inv_line_id.update({'quantity_done':inv_line_id.product_uom_qty})
        
        inv_id.button_validate()
        return inv_id
    
    def stock_qty_update(self,consume_ids):
        stock_qty = self.env['stock.change.product.qty']
        if not self.is_add_qty:
            if not self.is_updated:
                if consume_ids:
                    for consume_id in consume_ids:
                        pp_id = self.env['product.product'].search([('product_tmpl_id','=',consume_id.product_id.id)])
                        new_id = stock_qty.create({'product_id':pp_id.id,'product_tmpl_id':consume_id.product_id.id,'new_quantity':consume_id.product_qty})
                        new_id.change_product_qty()
                self.is_updated = True
                if self.lot_id:
                    lot_id = self.lot_id
                    lot_id.write({'status':'u'})
        else:
            if not self.is_updated:
                self.update_qty1()
                self.is_updated = True
         
    
    def update_qty(self):
        
        consume_ids = self.consume_ids
        consume_bom_ids = self.consume_bom_ids
        
        self.stock_qty_update(consume_ids)
        self.stock_qty_update(consume_bom_ids)
        
    def update_qty1(self):
        partner = self.env['res.partner']
        product = self.env['product.product']
        product_template = self.env['product.template']
        sale = self.env['sale.order']
        sale_line = self.env['sale.order.line']
        purchase = self.env['purchase.order']
        purchase_line = self.env['purchase.order.line']
        stock = self.env['stock.move']
        move_line = self.env['stock.move.line']
        
        lot_id = self.lot_id
        
        partner_id = partner.search([('name','=','Meat Center')])
        if not partner_id:
            partner_id = partner.create({'name':'Meat Center'})
            
        purchase_value = {'name':'New','partner_id':partner_id.id,'is_meat_center':True}
        purchase_id = purchase.create(purchase_value)
        purchse_line_value = self.purchase_lines_value(purchase_id)
        purchase_line_id = purchase_line.create(purchse_line_value)
        is_cut_material = True
        purchase_id.button_confirm(is_cut_material)
        inv_id = self.update_inv_details(purchase_id)
        
        
        sales_value = {'name':'New','partner_id':partner_id.id,'validity_date':datetime.now().date(),'is_meat_center':True}
        sale_id = sale.create(sales_value)
        sale_line_value = self.sale_lines_value(sale_id)
        sale_line_id = sale_line.create(sale_line_value)
        sale_id.action_confirm()
        inv_id = self.update_inv_details(sale_id)
        
        lot_id.write({'status':'u'})
        
        
    
    product_id = fields.Many2one('product.template','Product',required=True)
    product_qty = fields.Integer('Product Qty',default=1,required=True)
    total_weight = fields.Float("Total Weight",required=True)
    is_updated = fields.Boolean('Is Update?')
    consume_ids = fields.One2many("consume.details","meat_center_id",'Consume Details')
    consume_bom_ids = fields.One2many('consume.bom.details','meat_center_id','')
    is_add_qty = fields.Boolean('Is Add QTY?')
    lot_id = fields.Many2one('purchase.lot',string='Lot Number')
    
    @api.onchange('lot_id')
    def lot_weight(self):
        stock_move_line_id = self.env['stock.move.line'].search([('lot_name','=',self.lot_id.name)],limit=1)
        self.total_weight = stock_move_line_id.weight
    
    def write(self, values):
        if values:
            if "is_update" not in values:
                if self.is_updated:
                    values['is_updated']=False
            res = super(MeatCenter, self).write(values)
        return self
        
    
    
    