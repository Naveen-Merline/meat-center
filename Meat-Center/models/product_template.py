from odoo import models, fields, api, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    consume_ids = fields.One2many("consume.details","product_id",'Consume Details')
    
class ConsumeDetails(models.Model):
    _name = 'consume.details'
    
    
    @api.onchange('product_qty')
    def calculate_persontage(self):
        product_qty = self.product_qty
        self.persontage = (product_qty * 100)/self.total_weight
    
    product_id = fields.Many2one('product.template','Product',required=True)
    product_qty = fields.Float('Qty',default=1,required=True)
    total_weight = fields.Float('Total Weight')
    uom_id = fields.Many2one('uom.uom','Unit of Measure',required=True)
    meat_center_id = fields.Many2one('meat.center','Meat Center')
    persontage = fields.Float("Percentage")
    
class ConsumeBomDetails(models.Model):
    _name = 'consume.bom.details'
    
    @api.onchange('product_qty')
    def calculate_persontage(self):
        product_qty = self.product_qty
        self.persontage = (product_qty * 100)/self.total_weight
    
    product_id = fields.Many2one('product.template','Product',required=True)
    product_qty = fields.Float('Qty',default=1,required=True)
    total_weight = fields.Float('Total Weight')
    uom_id = fields.Many2one('uom.uom','Unit of Measure',required=True)
    meat_center_id = fields.Many2one('meat.center','Meat Center')
    persontage = fields.Float("Percentage")